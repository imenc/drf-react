-- MySQL dump 10.13  Distrib 5.7.30, for macos10.14 (x86_64)
--
-- Host: localhost    Database: proshop
-- ------------------------------------------------------
-- Server version	5.7.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add product',7,'add_product'),(26,'Can change product',7,'change_product'),(27,'Can delete product',7,'delete_product'),(28,'Can view product',7,'view_product'),(29,'Can add shipping address',8,'add_shippingaddress'),(30,'Can change shipping address',8,'change_shippingaddress'),(31,'Can delete shipping address',8,'delete_shippingaddress'),(32,'Can view shipping address',8,'view_shippingaddress'),(33,'Can add order',9,'add_order'),(34,'Can change order',9,'change_order'),(35,'Can delete order',9,'delete_order'),(36,'Can view order',9,'view_order'),(37,'Can add review',10,'add_review'),(38,'Can change review',10,'change_review'),(39,'Can delete review',10,'delete_review'),(40,'Can view review',10,'view_review'),(41,'Can add order item',11,'add_orderitem'),(42,'Can change order item',11,'change_orderitem'),(43,'Can delete order item',11,'delete_orderitem'),(44,'Can view order item',11,'view_orderitem');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$320000$uc9oX1pHUsbXcG387o7Uar$ZkGhpLYeQidnbxy2eOQii901nmd8lkhI8+Ey+EDtYRM=','2022-05-10 08:45:27.690909',1,'admin','','','admin@domain.com',1,1,'2022-03-16 15:19:11.491451'),(2,'pbkdf2_sha256$320000$9nsg4wTanvqO0uwh2YRfec$KMGMeJBdHoKvuEhPrHw+T9mwe2xNsRSNPyjyIHk6xw0=',NULL,0,'imenc@live.com','imenc','.','imenc@live.com',1,1,'2022-03-17 14:34:51.000000'),(3,'pbkdf2_sha256$320000$Aku1YFOB6EKITl3Xy3C5O5$nFQYmFs8qVHCcmG9UV8je/CdRfZ/Pp5L96gWmgC43Mw=',NULL,0,'john@email.com','John Doe','','john@email.com',0,1,'2022-03-24 05:14:19.173724'),(6,'pbkdf2_sha256$320000$bY2awVoPEaxfiZO3mYLnzq$jrBFLqD7MpmqifDB3LTnPfMm8pAHTvF3mg5cZkZN5W8=',NULL,0,'kardiman@email.com','Kardiman Aja','','kardiman@email.com',0,1,'2022-03-24 05:30:09.212336'),(8,'pbkdf2_sha256$320000$fcuR0qTm45Jfp7OSycuJRr$JxwqyD4cIvyeqWJ/GIK/kvycq/79Q9Rx7da8h7o96qc=',NULL,0,'sunyoto@email.com','Sunyoto  Kardiman','','sunyoto@email.com',0,1,'2022-03-25 08:48:00.243339'),(9,'pbkdf2_sha256$320000$v04QE7TOb9dZXIRtHB2YYD$VSubJfo2hkeQE/eVP49aJyevfjX5BROZoRuzOItfGFc=',NULL,0,'indrakenz@domain.com','Indra Kenz','','indrakenz@domain.com',0,1,'2022-03-27 13:21:51.578531'),(15,'pbkdf2_sha256$320000$EyJo6Omvle2LJuMotewibU$O2ZujoJNEkksNymHnOKRSKRVwyZYDvtq11CYCDU+SBc=',NULL,0,'user@email.com','User 1','','user@email.com',0,1,'2022-04-14 02:25:32.000000');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_order`
--

DROP TABLE IF EXISTS `base_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_order` (
  `paymentMethod` varchar(200) DEFAULT NULL,
  `taxPrice` decimal(7,2) DEFAULT NULL,
  `shippingPrice` decimal(7,2) DEFAULT NULL,
  `totalPrice` decimal(7,2) DEFAULT NULL,
  `isPaid` tinyint(1) NOT NULL,
  `paidAt` datetime(6) DEFAULT NULL,
  `isDelivered` tinyint(1) NOT NULL,
  `deliveredAt` datetime(6) DEFAULT NULL,
  `createdAt` datetime(6) NOT NULL,
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  KEY `base_order_user_id_8ad0adec_fk_auth_user_id` (`user_id`),
  CONSTRAINT `base_order_user_id_8ad0adec_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_order`
--

LOCK TABLES `base_order` WRITE;
/*!40000 ALTER TABLE `base_order` DISABLE KEYS */;
INSERT INTO `base_order` VALUES ('PayPal',250.92,0.00,3310.86,0,NULL,0,NULL,'2022-04-03 16:32:07.585407',1,6),('PayPal',167.28,0.00,2207.24,0,NULL,0,NULL,'2022-04-04 15:36:58.268969',2,6),('PayPal',167.28,0.00,2207.24,0,NULL,0,NULL,'2022-04-05 01:19:51.909378',3,6),('PayPal',167.28,0.00,2207.24,0,NULL,0,NULL,'2022-04-05 01:20:28.523433',4,6),('PayPal',167.28,0.00,2207.24,0,NULL,0,NULL,'2022-04-05 01:25:51.085418',5,6),('PayPal',167.28,0.00,2207.24,0,NULL,0,NULL,'2022-04-07 14:53:33.949264',6,6),('PayPal',167.28,0.00,2207.24,0,NULL,0,NULL,'2022-04-08 06:28:29.239320',7,6),('PayPal',7.38,10.00,107.37,1,'2022-05-11 07:36:58.000000',1,'2022-05-11 07:42:00.498659','2022-04-09 07:52:23.349103',8,6);
/*!40000 ALTER TABLE `base_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_orderitem`
--

DROP TABLE IF EXISTS `base_orderitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_orderitem` (
  `name` varchar(200) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` decimal(7,2) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  KEY `base_orderitem_order_id_aaa7f08a_fk_base_order__id` (`order_id`),
  KEY `base_orderitem_product_id_397c77c4_fk_base_product__id` (`product_id`),
  CONSTRAINT `base_orderitem_order_id_aaa7f08a_fk_base_order__id` FOREIGN KEY (`order_id`) REFERENCES `base_order` (`_id`),
  CONSTRAINT `base_orderitem_product_id_397c77c4_fk_base_product__id` FOREIGN KEY (`product_id`) REFERENCES `base_product` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_orderitem`
--

LOCK TABLES `base_orderitem` WRITE;
/*!40000 ALTER TABLE `base_orderitem` DISABLE KEYS */;
INSERT INTO `base_orderitem` VALUES ('Airpods Wireless Bluetooth Headphones',3,89.99,'/images/airpods.jpg',1,1,1),('Cannon EOS 80D DSLR Camera',3,929.99,'/images/camera.jpg',2,1,3),('Airpods Wireless Bluetooth Headphones',2,89.99,'/images/airpods.jpg',3,2,1),('Cannon EOS 80D DSLR Camera',2,929.99,'/images/camera.jpg',4,2,3),('Airpods Wireless Bluetooth Headphones',2,89.99,'/images/airpods.jpg',5,3,1),('Cannon EOS 80D DSLR Camera',2,929.99,'/images/camera.jpg',6,3,3),('Airpods Wireless Bluetooth Headphones',2,89.99,'/images/airpods.jpg',7,4,1),('Cannon EOS 80D DSLR Camera',2,929.99,'/images/camera.jpg',8,4,3),('Airpods Wireless Bluetooth Headphones',2,89.99,'/images/airpods.jpg',9,5,1),('Cannon EOS 80D DSLR Camera',2,929.99,'/images/camera.jpg',10,5,3),('Airpods Wireless Bluetooth Headphones',2,89.99,'/images/airpods.jpg',11,6,1),('Cannon EOS 80D DSLR Camera',2,929.99,'/images/camera.jpg',12,6,3),('Airpods Wireless Bluetooth Headphones',2,89.99,'/images/airpods.jpg',13,7,1),('Cannon EOS 80D DSLR Camera',2,929.99,'/images/camera.jpg',14,7,3),('Airpods Wireless Bluetooth Headphones',1,89.99,'/images/airpods.jpg',15,8,1);
/*!40000 ALTER TABLE `base_orderitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_product`
--

DROP TABLE IF EXISTS `base_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_product` (
  `name` varchar(200) DEFAULT NULL,
  `brand` varchar(200) DEFAULT NULL,
  `category` varchar(200) DEFAULT NULL,
  `description` longtext,
  `rating` decimal(7,2) DEFAULT NULL,
  `numReviews` int(11) DEFAULT NULL,
  `price` decimal(7,2) DEFAULT NULL,
  `countInStock` int(11) DEFAULT NULL,
  `createdAt` datetime(6) NOT NULL,
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  KEY `base_product_user_id_95204f5a_fk_auth_user_id` (`user_id`),
  CONSTRAINT `base_product_user_id_95204f5a_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_product`
--

LOCK TABLES `base_product` WRITE;
/*!40000 ALTER TABLE `base_product` DISABLE KEYS */;
INSERT INTO `base_product` VALUES ('Airpods Wireless Bluetooth Headphones','Apple','Electronics','Bluetooth technology lets you connect it with compatible devices wirelessly High-quality AAC audio offers immersive listening experience Built-in microphone allows you to take calls while working',3.00,1,5.00,9,'2022-03-17 14:37:40.352680',1,2,'airpods.jpg'),('iPhone 11 Pro 256GB Memory','Apple','Electronics','Introducing the iPhone 11 Pro. A transformative triple-camera system that adds tons of capability without complexity. An unprecedented leap in battery life',5.00,1,599.99,12,'2022-03-17 15:50:29.354625',2,2,'phone.jpg'),('Cannon EOS 80D DSLR Camera','Cannon','Electronics','Characterized by versatile imaging specs, the Canon EOS 80D further clarifies itself using a pair of robust focusing systems and an intuitive design',3.00,12,929.99,10,'2022-03-17 15:52:16.683481',3,2,'camera.jpg'),('Sony Playstation 4 Pro White Version','Sony','Electronics','The ultimate home entertainment center starts with PlayStation. Whether you are into gaming, HD movies, television, music',5.00,12,399.99,12,'2022-03-17 15:54:40.655539',4,2,'playstation.jpg'),('Logitech G-Series Gaming Mouse','Logitech','Electronics','Get a better handle on your games with this Logitech LIGHTSYNC gaming mouse. The six programmable buttons allow customization for a smooth playing experience',3.50,10,49.99,7,'2022-03-17 15:56:30.464622',5,2,'mouse.jpg'),('Amazon Echo Dot 3rd Generation','Amazon','Electronics','Meet Echo Dot - Our most popular smart speaker with a fabric design. It is our most compact smart speaker that fits perfectly into small space',4.00,12,29.99,10,'2022-03-17 16:03:10.416122',6,2,'alexa.jpg'),('Sample One','Sample Brand','Sample Category','Some Description',NULL,0,0.00,0,'2022-04-24 10:36:54.191297',12,2,'Ardi05.png'),('Sample Two','Sample Brand','Sample Category','',NULL,0,0.00,0,'2022-04-24 15:02:45.444962',13,2,'HRV.png'),('Sample Name','Sample Brand','Sample Category','',NULL,0,0.00,0,'2022-05-16 07:00:12.929347',14,2,'/placeholder.png');
/*!40000 ALTER TABLE `base_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_review`
--

DROP TABLE IF EXISTS `base_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_review` (
  `name` varchar(200) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `comment` longtext,
  `createdAt` datetime(6) NOT NULL,
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  KEY `base_review_product_id_9a95f2eb_fk_base_product__id` (`product_id`),
  KEY `base_review_user_id_9a731b84_fk_auth_user_id` (`user_id`),
  CONSTRAINT `base_review_product_id_9a95f2eb_fk_base_product__id` FOREIGN KEY (`product_id`) REFERENCES `base_product` (`_id`),
  CONSTRAINT `base_review_user_id_9a731b84_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_review`
--

LOCK TABLES `base_review` WRITE;
/*!40000 ALTER TABLE `base_review` DISABLE KEYS */;
INSERT INTO `base_review` VALUES ('imenc',3,'Good product','2022-05-12 09:10:27.391463',1,1,2),('imenc',5,'Larang coeg','2022-05-14 12:00:37.541376',4,2,2);
/*!40000 ALTER TABLE `base_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_shippingaddress`
--

DROP TABLE IF EXISTS `base_shippingaddress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_shippingaddress` (
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `postalCode` varchar(200) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `shippingPrice` decimal(7,2) DEFAULT NULL,
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `order_id` (`order_id`),
  CONSTRAINT `base_shippingaddress_order_id_d85223e7_fk_base_order__id` FOREIGN KEY (`order_id`) REFERENCES `base_order` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_shippingaddress`
--

LOCK TABLES `base_shippingaddress` WRITE;
/*!40000 ALTER TABLE `base_shippingaddress` DISABLE KEYS */;
INSERT INTO `base_shippingaddress` VALUES ('Kasihan Tamantirto','Bantul','55252','Indonesia',NULL,1,1),('Kasihan Tamantirto','Bantul','55252','Indonesia',NULL,2,2),('Kasihan Tamantirto','Bantul','55252','Indonesia',NULL,3,3),('Kasihan Tamantirto','Bantul','55252','Indonesia',NULL,4,4),('Kasihan Tamantirto','Bantul','55252','Indonesia',NULL,5,5),('Kasihan Tamantirto','Bantul','55252','Indonesia',NULL,6,6),('Kasihan Tamantirto','Bantul','55252','Indonesia',NULL,7,7),('Kasihan Tamantirto','Bantul','55252','Indonesia',NULL,8,8);
/*!40000 ALTER TABLE `base_shippingaddress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2022-03-17 14:34:51.445346','2','imenc',1,'[{\"added\": {}}]',4,1),(2,'2022-03-17 14:35:27.605422','2','imenc',2,'[{\"changed\": {\"fields\": [\"First name\", \"Last name\", \"Email address\", \"Staff status\"]}}]',4,1),(3,'2022-03-17 14:37:40.355537','1','Airpods Wireless Bluetooth Headphones',1,'[{\"added\": {}}]',7,1),(4,'2022-03-17 14:38:01.535204','1','Airpods Wireless Bluetooth Headphones',2,'[]',7,1),(5,'2022-03-17 15:50:29.356451','2','iPhone 11 Pro 256GB Memory',1,'[{\"added\": {}}]',7,1),(6,'2022-03-17 15:52:16.686219','3','Cannon EOS 80D DSLR Camera',1,'[{\"added\": {}}]',7,1),(7,'2022-03-17 15:52:46.749764','3','Cannon EOS 80D DSLR Camera',2,'[{\"changed\": {\"fields\": [\"Image\"]}}]',7,1),(8,'2022-03-17 15:53:23.891445','3','Cannon EOS 80D DSLR Camera',2,'[]',7,1),(9,'2022-03-17 15:54:40.656400','4','Sony Playstation 4 Pro White Version',1,'[{\"added\": {}}]',7,1),(10,'2022-03-17 15:56:30.466124','5','Logitech G-Series Gaming Mouse',1,'[{\"added\": {}}]',7,1),(11,'2022-03-17 16:03:10.416922','6','Amazon Echo Dot 3rd Generation',1,'[{\"added\": {}}]',7,1),(12,'2022-03-24 14:09:22.162644','2','imenc@live.com',2,'[]',4,1),(13,'2022-03-27 04:58:54.007164','2','imenc@live.com',2,'[{\"changed\": {\"fields\": [\"First name\", \"Last name\"]}}]',4,1),(14,'2022-03-28 04:19:11.411855','2','user4@domain.com',2,'[{\"changed\": {\"fields\": [\"password\"]}}]',4,1),(15,'2022-03-28 04:19:35.605456','2','imenc@live.com',2,'[{\"changed\": {\"fields\": [\"Username\", \"First name\", \"Email address\"]}}]',4,1),(16,'2022-03-30 13:15:57.751812','6','kardiman@email.com',2,'[{\"changed\": {\"fields\": [\"password\"]}}]',4,1),(17,'2022-04-09 07:46:26.486306','6','Amazon Echo Dot 3rd Generation',2,'[{\"changed\": {\"fields\": [\"CountInStock\"]}}]',7,1),(18,'2022-04-09 07:46:31.168156','5','Logitech G-Series Gaming Mouse',2,'[]',7,1),(19,'2022-04-09 07:46:37.726190','4','Sony Playstation 4 Pro White Version',2,'[]',7,1),(20,'2022-04-09 07:46:49.047130','3','Cannon EOS 80D DSLR Camera',2,'[{\"changed\": {\"fields\": [\"CountInStock\"]}}]',7,1),(21,'2022-04-09 07:46:55.504870','2','iPhone 11 Pro 256GB Memory',2,'[{\"changed\": {\"fields\": [\"CountInStock\"]}}]',7,1),(22,'2022-04-09 07:47:02.344582','1','Airpods Wireless Bluetooth Headphones',2,'[{\"changed\": {\"fields\": [\"CountInStock\"]}}]',7,1),(23,'2022-04-14 02:06:35.047643','13','user1',1,'[{\"added\": {}}]',4,1),(24,'2022-04-14 02:06:54.789822','13','user1',2,'[{\"changed\": {\"fields\": [\"First name\"]}}]',4,1),(25,'2022-04-14 02:08:26.036802','13','user1@email.com',2,'[{\"changed\": {\"fields\": [\"Username\", \"Email address\"]}}]',4,1),(26,'2022-04-14 02:09:05.804889','14','user2@email.com',1,'[{\"added\": {}}]',4,1),(27,'2022-04-14 02:09:21.173755','14','user2@email.com',2,'[{\"changed\": {\"fields\": [\"First name\"]}}]',4,1),(28,'2022-04-14 02:09:34.268672','14','user2@email.com',2,'[{\"changed\": {\"fields\": [\"Email address\"]}}]',4,1),(29,'2022-04-14 02:25:33.186170','15','user1@email.com',1,'[{\"added\": {}}]',4,1),(30,'2022-04-14 02:25:50.311295','15','user1@email.com',2,'[{\"changed\": {\"fields\": [\"First name\", \"Email address\"]}}]',4,1),(31,'2022-04-14 02:26:37.368773','16','user2@email.com',1,'[{\"added\": {}}]',4,1),(32,'2022-04-14 05:51:32.934287','16','user2@email.com',2,'[{\"changed\": {\"fields\": [\"First name\", \"Email address\"]}}]',4,1),(33,'2022-04-16 08:17:00.490273','7','Produk abal²',1,'[{\"added\": {}}]',7,1),(34,'2022-04-16 08:30:11.810002','7','Produk abal²',2,'[{\"changed\": {\"fields\": [\"Rating\"]}}]',7,1),(35,'2022-04-24 06:55:24.504992','7','Test',1,'[{\"added\": {}}]',7,1),(36,'2022-05-10 08:45:46.661530','8','2022-04-09 07:52:23.349103+00:00',2,'[{\"changed\": {\"fields\": [\"IsPaid\"]}}]',9,1),(37,'2022-05-10 08:46:08.136414','8','2022-04-09 07:52:23.349103+00:00',2,'[{\"changed\": {\"fields\": [\"IsPaid\"]}}]',9,1),(38,'2022-05-10 08:53:51.420977','1','2022-04-03 16:32:07.585407+00:00',2,'[{\"changed\": {\"fields\": [\"IsPaid\"]}}]',9,1),(39,'2022-05-10 08:54:06.674338','1','2022-04-03 16:32:07.585407+00:00',2,'[]',9,1),(40,'2022-05-10 08:54:31.350508','8','2022-04-09 07:52:23.349103+00:00',2,'[{\"changed\": {\"fields\": [\"IsPaid\"]}}]',9,1),(41,'2022-05-11 04:00:40.524072','8','2022-04-09 07:52:23.349103+00:00',2,'[{\"changed\": {\"fields\": [\"IsPaid\"]}}]',9,1),(42,'2022-05-11 04:01:26.964406','8','2022-04-09 07:52:23.349103+00:00',2,'[{\"changed\": {\"fields\": [\"PaidAt\"]}}]',9,1),(43,'2022-05-11 04:20:39.687181','8','2022-04-09 07:52:23.349103+00:00',2,'[{\"changed\": {\"fields\": [\"IsPaid\"]}}]',9,1),(44,'2022-05-11 04:21:21.233170','8','2022-04-09 07:52:23.349103+00:00',2,'[]',9,1),(45,'2022-05-11 04:30:47.525675','8','2022-04-09 07:52:23.349103+00:00',2,'[{\"changed\": {\"fields\": [\"IsPaid\", \"PaidAt\"]}}]',9,1),(46,'2022-05-11 04:30:55.306565','1','2022-04-03 16:32:07.585407+00:00',2,'[{\"changed\": {\"fields\": [\"IsPaid\"]}}]',9,1),(47,'2022-05-11 04:45:12.259232','8','2022-04-09 07:52:23.349103+00:00',2,'[{\"changed\": {\"fields\": [\"IsPaid\"]}}]',9,1),(48,'2022-05-11 04:45:29.492964','8','2022-04-09 07:52:23.349103+00:00',2,'[{\"changed\": {\"fields\": [\"PaidAt\"]}}]',9,1),(49,'2022-05-11 07:33:08.455484','8','2022-04-09 07:52:23.349103+00:00',2,'[{\"changed\": {\"fields\": [\"IsPaid\", \"PaidAt\"]}}]',9,1),(50,'2022-05-11 07:35:17.982336','8','2022-04-09 07:52:23.349103+00:00',2,'[{\"changed\": {\"fields\": [\"IsDelivered\", \"DeliveredAt\"]}}]',9,1),(51,'2022-05-11 07:35:53.047837','8','2022-04-09 07:52:23.349103+00:00',2,'[{\"changed\": {\"fields\": [\"IsPaid\"]}}]',9,1),(52,'2022-05-11 07:37:00.606586','8','2022-04-09 07:52:23.349103+00:00',2,'[{\"changed\": {\"fields\": [\"PaidAt\"]}}]',9,1),(53,'2022-05-11 07:39:46.421857','8','2022-04-09 07:52:23.349103+00:00',2,'[]',9,1),(54,'2022-05-14 09:03:47.664849','2','5',3,'',10,1),(55,'2022-05-14 10:27:50.163619','3','3',3,'',10,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(9,'base','order'),(11,'base','orderitem'),(7,'base','product'),(10,'base','review'),(8,'base','shippingaddress'),(5,'contenttypes','contenttype'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2022-03-16 15:10:22.150078'),(2,'auth','0001_initial','2022-03-16 15:10:22.362058'),(3,'admin','0001_initial','2022-03-16 15:10:22.403950'),(4,'admin','0002_logentry_remove_auto_add','2022-03-16 15:10:22.412722'),(5,'admin','0003_logentry_add_action_flag_choices','2022-03-16 15:10:22.424456'),(6,'contenttypes','0002_remove_content_type_name','2022-03-16 15:10:22.466172'),(7,'auth','0002_alter_permission_name_max_length','2022-03-16 15:10:22.482805'),(8,'auth','0003_alter_user_email_max_length','2022-03-16 15:10:22.500474'),(9,'auth','0004_alter_user_username_opts','2022-03-16 15:10:22.509807'),(10,'auth','0005_alter_user_last_login_null','2022-03-16 15:10:22.530103'),(11,'auth','0006_require_contenttypes_0002','2022-03-16 15:10:22.531974'),(12,'auth','0007_alter_validators_add_error_messages','2022-03-16 15:10:22.539021'),(13,'auth','0008_alter_user_username_max_length','2022-03-16 15:10:22.554813'),(14,'auth','0009_alter_user_last_name_max_length','2022-03-16 15:10:22.574905'),(15,'auth','0010_alter_group_name_max_length','2022-03-16 15:10:22.589888'),(16,'auth','0011_update_proxy_permissions','2022-03-16 15:10:22.598766'),(17,'auth','0012_alter_user_first_name_max_length','2022-03-16 15:10:22.612652'),(18,'sessions','0001_initial','2022-03-16 15:10:22.635262'),(19,'base','0001_initial','2022-03-16 15:24:05.132131'),(20,'base','0002_order_shippingaddress_review_orderitem','2022-03-16 15:55:03.912760'),(21,'base','0003_product_image','2022-03-17 01:07:38.083990');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('89b1k0fioyel52w8anqd5ei5k343c2k2','.eJxVjEEOwiAQRe_C2hBGKFCX7j0DAWZGqgaS0q6Md7dNutDtf-_9twhxXUpYO81hQnERIE6_W4r5SXUH-Ij13mRudZmnJHdFHrTLW0N6XQ_376DEXrZaEzh0ltkrg9kAkGYFmmjkjGfPTIO30eoEFjTrbE3anOR4YEY1JvH5AvP6OJg:1noLUl:8cM64WEFdY26jDls2Z6f7jkh-pwBNG_MmJf83t01lxI','2022-05-24 08:45:27.697605'),('adab51wki37usgvjowmqfv7oct09735r','.eJxVjEEOwiAQRe_C2hBGKFCX7j0DAWZGqgaS0q6Md7dNutDtf-_9twhxXUpYO81hQnERIE6_W4r5SXUH-Ij13mRudZmnJHdFHrTLW0N6XQ_376DEXrZaEzh0ltkrg9kAkGYFmmjkjGfPTIO30eoEFjTrbE3anOR4YEY1JvH5AvP6OJg:1nZYB3:FsmRASlZ8dG3Vjw6iZe_rbE-0dEPU9ko2RddsF4VyUs','2022-04-13 13:15:57.761730'),('f80htv49jkmatg5ta7d2505rshl82fts','.eJxVjEEOwiAQRe_C2hBGKFCX7j0DAWZGqgaS0q6Md7dNutDtf-_9twhxXUpYO81hQnERIE6_W4r5SXUH-Ij13mRudZmnJHdFHrTLW0N6XQ_376DEXrZaEzh0ltkrg9kAkGYFmmjkjGfPTIO30eoEFjTrbE3anOR4YEY1JvH5AvP6OJg:1neos0:i_IdlcplHFfHycdK2GDiuLWOe8s-dIhPrtMuppUkLcQ','2022-04-28 02:06:04.853940'),('i0t3fs07wzoks3ohdhtb4lfgnt2yx3ex','.eJxVjEEOwiAQRe_C2hBGKFCX7j0DAWZGqgaS0q6Md7dNutDtf-_9twhxXUpYO81hQnERIE6_W4r5SXUH-Ij13mRudZmnJHdFHrTLW0N6XQ_376DEXrZaEzh0ltkrg9kAkGYFmmjkjGfPTIO30eoEFjTrbE3anOR4YEY1JvH5AvP6OJg:1nUrCO:KvC2hc27jpXfkiGyWVFBVJrvIXvySS4435he19Yn-hw','2022-03-31 14:33:56.981255'),('pvfhjsxkvu6wr1cbhqfj6za4u1pcrlf3','.eJxVjEEOwiAQRe_C2hBGKFCX7j0DAWZGqgaS0q6Md7dNutDtf-_9twhxXUpYO81hQnERIE6_W4r5SXUH-Ij13mRudZmnJHdFHrTLW0N6XQ_376DEXrZaEzh0ltkrg9kAkGYFmmjkjGfPTIO30eoEFjTrbE3anOR4YEY1JvH5AvP6OJg:1nUVz0:AKdQNNnIAPPbx7mxfHEVgYRFxI5iK6uFRcNAvYnFxVQ','2022-03-30 15:54:42.266203'),('xutftvhl11r9wwszajm5es9yvg5ww5hb','.eJxVjEEOwiAQRe_C2hBGKFCX7j0DAWZGqgaS0q6Md7dNutDtf-_9twhxXUpYO81hQnERIE6_W4r5SXUH-Ij13mRudZmnJHdFHrTLW0N6XQ_376DEXrZaEzh0ltkrg9kAkGYFmmjkjGfPTIO30eoEFjTrbE3anOR4YEY1JvH5AvP6OJg:1nUVSZ:2lkIPQGJR4vb-XmL03uTSBmHlZXArQhn5vUQsWfq46Y','2022-03-30 15:21:11.763088');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'proshop'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-25 13:44:10
