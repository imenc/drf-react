# DRF React
Django with React | An Ecommerce Website


## Udemy Course Page
[https://www.udemy.com/course/django-with-react-an-ecommerce-website](https://www.udemy.com/course/django-with-react-an-ecommerce-website)

## Course Git
[https://github.com/divanov11/proshop_django](https://github.com/divanov11/proshop_django)

## Project Folder
```bash
~/udemy/drf-react
~/udemy/drf-react/ecommerce 
~/udemy/drf-react/proshop_django 
~/udemy/drf-react/deployment
```

